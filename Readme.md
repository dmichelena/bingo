# Bingo

Para correr el proyecto
```
npm i
```
```
node app
```

## Servicios 
1. [GET] /

Solo devuelve un mensaje dummy

2. [POST] /reset

Inicializa el bingo con todas números deseleccionado

3. [POST] /number

Guarda en memoria un número nuevo del bingo

4. [POST] /generate-card

Genera una combinación de números única. Si no logra generar uno en 5 intentos lanza un error

5. [POST] /validate-card

Obtiene una cartilla por id (lanza error si no existe) y valida que todos los números ya salieron
