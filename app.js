const express = require('express');
const app = express();
const port = 3000;
const bingo = require('./services/bingo');
const cardRepository = require('./repository/card');

app.get('/', (req, res) => {
    res.send('Bingo Service!!!');
});

app.post('/reset', (req, res) => {
    bingo.reset();
    res.send('Let\'s play!!!');
});

app.post('/number', (req, res) => {
    res.send(bingo.callNumber().toString());
});

app.post('/generate-card', (req, res) => {
    res.send(JSON.stringify(cardRepository.generateCard()));
});

app.post('/validate-card', (req, res) => {
    const card = cardRepository.find(req.query.id);
    res.send(bingo.validate(card).toString());
});

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
});