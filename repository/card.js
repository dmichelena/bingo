//Simulate index in db
const dbIndexById = {};
const dbIndexByHash = {};

let balls = [];

class Card {

    generateCard() {
        let validHash = null;
        let id = null;
        //Try five times
        for (let i = 0; i < 5; i++) {
            const card = this.generate();
            const hash = card.join('-');
            if (!dbIndexByHash[hash]) {
                dbIndexByHash[hash] = true;
                id = Object.keys(dbIndexById).length;
                dbIndexById[id] = card;
                validHash = hash;
                break;
            }
        }
        if (id === null) {
            throw new Error('Generation error');
        }

        return {id, numbers: validHash};
    }

    generate() {
        const card = {};
        const ballsByLetter = {
            'B': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,],
            'I': [16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30,],
            'N': [31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45,],
            'G': [46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60,],
            'O': [61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75,],
        };
        for (const letter in ballsByLetter) {
            for(let i = 0; i < 5; i++) {
                const index = Math.floor(Math.random() * ballsByLetter[letter].length);
                const number = ballsByLetter[letter][index];
                ballsByLetter[letter].splice(index, 1);
                card[number] = true;
            }
        }

        return Object.keys(card);
    }

    find(id) {
        const card = dbIndexById[id];
        if (!card) {
            throw new Error('Card not found');
        }

        return {
            id,
            numbers: card,
        };
    }

}

module.exports = new Card();